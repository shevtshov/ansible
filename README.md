Commands



| Command | Description |
| ------ | ------ |
| Execute command: | ansible all -i hosts -m shell -a "uptime" |
| Ping | ansible  all -i hosts -m -ping |
| Create file: | ansible all -i hosts -m file -a "path=/home/ubuntu/ansible_test.txt state=touch" |
| Copy file: | ansible all -i hosts -m copy -a "src=file123 dest=/home/ubuntu mode=777" |
| Debug example: | ansible all -i hosts -m debug -a "var=dev" |
| Execute playbook: | ansible-playbook -i hosts ping.yml |
| Create role: | ansible-galaxy init first_setup |
| List all groups, servers, vars: | ansible-inventory --list |
| What related to which group | ansible-inventory --graph |
| ALL info about server | ansible dev -i hosts -m setup |
| AD-hoc command | ansible all -i hosts -m shell -a "ls -la /home | grep client* && pwd && env" |
| Install for apt | ansible all -m apt -a "name=itop" -b |
| Install for yum | ansible all -m yum -a "name=package_name" -b |
| Remove package | ansible all -m apt -a "name=itop state=absent |



https://www.educba.com/ansible-commands/
